package com.galvanize;
import org.junit.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import 
import org.junit.*;

import java.awt.geom.Point2D;

import static com.sun.org.apache.xerces.internal.util.PropertyState.is;
import static org.hamcrest.CoreMatchers.*;


public class SnakeTest {
    @Test
    @DisplayName("Snake should move")
    public void testSnakeMoves(){
        Snake snake = new Snake(new Point2D()(0,0));
        snake.setDirection(Direction.RIGHT);
        snake.update();

        assertThat(snake.getPosition(), is(new Point2D(1,0)));


    }
}
